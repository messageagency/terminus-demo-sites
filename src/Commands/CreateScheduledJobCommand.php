<?php

namespace Pantheon\TerminusDemoSites\Commands;

use Pantheon\Terminus\Exceptions\TerminusException;
use Pantheon\TerminusBuildTools\Commands\BuildToolsBase;
use Pantheon\TerminusClu\ServiceProviders\RepositoryProviders\CluGitTrait;

class CreateScheduledJobCommand extends BuildToolsBase {

  use CluGitTrait;

  /**
   * @var string
   */
  protected $target_project;

  /**
   * Create a scheduled job.
   *
   * @authorize
   *
   * @command project:create:schedule
   *
   * @aliases project:schedule schedule
   *
   * @param string $site_name Site name.
   *
   * @param string $pattern The cron pattern.
   *
   * @usage <site> Create a schedule for a given site.
   *
   */
  public function createScheduledJob($site_name, $pattern) {
    /** @var \Pantheon\Terminus\Models\Site $site */
    $site = $this->getSite($site_name);
    $buildMetadata = $this->retrieveBuildMetadata("$site_name.dev");
    $url = $this->getMetadataUrl($buildMetadata);
    $this->target_project = $this->projectFromRemoteUrl($url);

    // Create a Git repository service provider appropriate to the URL
    $this->createCIProvider('pipelines');

    // Ensure that credentials for the Git provider are available
    $this->providerManager()->validateCredentials();
    $repoApiUrl = "repositories/messageagency/$site_name";

    $this->logger->notice('Create a schedule with pattern ' . $pattern);

    $data = [
      'enabled' => TRUE,
      'cron_pattern' => $pattern,
      'target' => [
        'type' => 'pipeline_ref_target',
        'ref_type' => 'branch',
        'ref_name' => 'master',
        'selector' => [
          'type' => 'custom',
          'pattern' => 'demo',
        ],
      ],
    ];
    try {
      // Enable CLU scheduled task.
      $this->ci_provider->api()
        ->request("$repoApiUrl/pipelines_config/schedules/", $data, 'POST');
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
  }

}