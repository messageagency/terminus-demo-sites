<?php

namespace Pantheon\TerminusDemoSites\Commands;

use Pantheon\Terminus\Exceptions\TerminusException;
use Pantheon\TerminusBuildTools\Commands\BuildToolsBase;
use Pantheon\TerminusClu\ServiceProviders\RepositoryProviders\CluGitTrait;

/**
 * Provides commands to create sites for demos.
 */
class CreateDemoCommand extends BuildToolsBase {

  use CluGitTrait;

  /**
   * @var string
   */
  protected $target_project;

  /**
   * @var string
   */
  protected $working_dir;

  /**
   * @var \Pantheon\TerminusClu\ServiceProviders\RepositoryProviders\GitProvider
   */
  protected $cluProvider;

  /**
   * Create a new PR and site.
   *
   * @authorize
   *
   * @command project:create:demo
   *
   * @aliases project:demo demo
   *
   * @param string $site_name Site name.
   *
   * @usage <site> Create a pull request, thence a demo site.
   *
   */
  public function createDemo($site_name) {
    /** @var \Pantheon\Terminus\Models\Site $site */
    $site = $this->getSite($site_name);
    $buildMetadata = $this->retrieveBuildMetadata("$site_name.dev");
    $url = $this->getMetadataUrl($buildMetadata);
    $this->target_project = $this->projectFromRemoteUrl($url);

    // Create a Git repository service provider appropriate to the URL
    $this->inferGitProviderFromUrl($url);

    // Ensure that credentials for the Git provider are available
    $this->providerManager()->validateCredentials();

    if (!$this->needsDemo()) {
      return;
    }

    $this->cluProvider = $this->inferGitCluProviderFromUrl($url);
    $this->cluProvider->setCredentials($this->providerManager()
      ->credentialManager());

    // Create a working directory
    $this->working_dir = $this->tempdir('local-site');

    $this->cluProvider->cloneRepository($this->target_project, $this->working_dir);

    $date = date('Y-m-d-H-i');
    // Checkout a dated branch to make the commit
    $branch_name = 'demo-' . $date;
    $this->passthru("git -C {$this->working_dir} checkout -b $branch_name");

    $title = "Demo Environment ({$date})";
    $description = "New demo site --chat --skip-vr";
    $message = $title . PHP_EOL . $description;
    $this->passthru("git -C {$this->working_dir} commit --allow-empty -m " . escapeshellarg($message));
    $this->passthru("git -C {$this->working_dir} push origin $branch_name");
    $this->cluProvider->createPullRequest($this->target_project, $branch_name, $title, ['description' => $description]);

    // Clean up any environments older than 3 months.
    $this->cleanUpOldDemoBranches();
  }

  /**
   * Closes old demo branches, preserving most recent 3.
   *
   * @return void
   */
  protected function cleanUpOldDemoBranches() {
    $prs = $this->git_provider->branchesForPullRequests($this->target_project, 'open', NULL, 'branch');
    $demo_prs = [];
    foreach ($prs as $id => $branch) {
      $matches = [];
      if (preg_match('/^demo-([0-9-]+)/', $branch, $matches)) {
        $demo_prs[$id] = [
          'branch' => $branch,
          'timestamp' => strtotime($matches[1]),
        ];
      }
    }
    if (empty($demo_prs) || count($demo_prs) <= 3) {
      return;
    }

    uasort($demo_prs, function ($a, $b) {
      return $a['timestamp'] - $b['timestamp'];
    });

    $old_prs = array_slice($demo_prs, 0, count($demo_prs) - 3, TRUE);
    foreach ($old_prs as $id => $data) {
      $branch = $data['branch'];
      $this->log()
        ->notice("Found old demo branch: {branch}", [
          "branch" => $branch,
        ]);
      if (!$this->cluProvider->closePullRequest($this->target_project, $id)) {
        throw new TerminusException('Failed to close existing composer lock update PR');
      }
      $this->exec("git -C {$this->working_dir} push origin --delete $branch");
      $this->log()
        ->info("Closed existing PR {number} and branch {branch}", [
          'number' => $id,
          'branch' => $branch,
        ]);
    }
  }

  /**
   * Helper to determine whether to proceed with demo creation.
   */
  protected function needsDemo() {
    $prs = $this->git_provider->branchesForPullRequests($this->target_project, 'open', NULL, 'branch');
    $demo_prs = [];

    foreach ($prs as $id => $branch) {
      $matches = [];
      if (preg_match('/^demo-([0-9-]+)/', $branch, $matches)) {
        $demo_prs[$id] = [
          'branch' => $branch,
          'timestamp' => strtotime($matches[1]),
        ];
      }
    }

    if (empty($demo_prs) || count($demo_prs) < 3) {
      $this->log()
        ->notice("Found fewer than 3 demo branches.", [
          "branch" => $branch,
        ]);
      return TRUE;
    }

    foreach ($demo_prs as $data) {
      if ($data['timestamp'] < strtotime('-3 months')) {
        $this->log()
          ->notice("Found old demo branch: {branch}", [
            "branch" => $branch,
          ]);
        return TRUE;
      }
    }
    $this->log()
      ->notice("Already 3 demo branches, none older than 3 months.", [
        "branch" => $branch,
      ]);
    return FALSE;
  }

}